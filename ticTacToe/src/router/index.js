import Vue from 'vue'
import Router from 'vue-router'
import Game from '@/components/Game'
import SelectPlayers from '@/components/GameSelectPlayers'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'GameSelectPlayers',
      component: SelectPlayers
    },

    {
      path: '/ticTacToe',
      name: 'Game',
      component: Game
    }
  ]
})
