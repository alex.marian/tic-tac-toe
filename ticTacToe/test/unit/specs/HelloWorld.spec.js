import Vue from 'vue'
import SelectPlayers from '@/components/SelectPlayers'

describe('SelectPlayers.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(SelectPlayers)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('label[for="player1"]').textContent)
      .to.equal('Player 1 name:')
    expect(vm.$el.querySelector('label[for="player2"]').textContent)
      .to.equal('Player 2 name:')
  })
})
